import React from "react";
import ReactDOM from "react-dom";
import { ParallaxProvider } from 'react-scroll-parallax';
import Navbar from './scene/navbar';
import Home from './scene/home';
import Brandband from './scene/brandband';
import Description from './scene/description';
import Carousel from './scene/carousel';
import Savings from './scene/savings';
import Statsband from './scene/statsband';
import Contact from './scene/contact';
import Footer from './scene/footer';
import Copyright from './scene/copyright';
import './main.scss'
const Index = () => {
    return (<div>
            <Navbar/>
            <Home/>
            <Brandband/>
            <Description/>
            <Carousel/>
            <Savings/>
			<ParallaxProvider>
				<Statsband/>
			</ParallaxProvider>
			<Contact/>
			<Footer/>
			<Copyright/>
        </div>
    )
}

ReactDOM.render(<Index/>, document.getElementById("index"));
