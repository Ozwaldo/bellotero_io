import React, {
    Component
} from 'react';
import './footer.scss';
import logoAppStore from 'assets/google-play.png'
import logoPlayStore from 'assets/app-store.svg'
class Footer extends Component {
    render() {
        return <footer className="footer" id="Footer" >
                <div className="container">
					<div className="columns is-mobile">
						<div className="column is-hidden-mobile is-one-quarter-tablet"><img id="logoWhite" src={require('assets/bellotero-logo-white.svg')} alt="Logo white"/></div>
                        <div className="column is-hidden-mobile is-one-quarter-tablet">
                            <span className="subtitle">Bellotero.io</span>
                            <ul>
                                <li>
                                    <a href="#">Features</a>
                                </li>
                                <li>
                                    <a href="#">Solutions</a>
                                </li>
                                <li>
                                    <a href="#">Stories</a>
                                </li>
                                <li>
                                    <a href="#">About</a>
                                </li>
                                <li>
                                    <a href="#">Blog</a>
                                </li>
                            </ul>
                        </div>
                        <div className="column is-half-mobile is-one-quarter-tablet">
                            <span className="subtitle">Social</span>
                            <ul>
                                <li>
                                    <a href="https://www.facebook.com/electriceverywhere/" target="_blank" >Facebook</a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/elonmusk" target="_blank"> Twitter</a>
                                </li>
                                <li>
                                <a href="https://mx.linkedin.com/company/tesla-motors" target="_blank"> LinkedIn</a>
                                </li>
                                <li>
                                <a href="https://www.instagram.com/elonmusk" target="_blank">Instagram</a>
                                </li>
                            </ul>
                        </div>
                        <div className="column is-half-mobile is-one-quarter-tablet">
                            <span className="subtitle">Support</span>
                            <ul>
                                <li>
                                    <a href="mailto:support@bellotero.com" >support@bellotero.com</a>
                                </li>
                                <li>
                                    <a href="tel:(555)555-5555" >(555) 555-5555</a>
                                </li>
                                <li>
                                    <a href="#" >Chat now</a>
                                </li>
                                <img className="store" src={logoAppStore} />
                                <img className="store" src={logoPlayStore} />
                            </ul>
                        </div>
					</div>
                </div>
            </footer>
    }
}

export default Footer;
