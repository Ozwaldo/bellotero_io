import React, {
    Component
} from 'react';
import './savings.scss';
import 'bulma-slider/dist/js/bulma-slider.min.js';
import 'bulma-slider/dist/css/bulma-slider.min.css';

class Saving extends Component {
    constructor(props) {
        super(props)
        this.state = {
			monthly: 10,
			employees: 1,
			estimated:3,
			savings:1340,
		}
        this.handleMonthlyChange = this.handleMonthlyChange.bind(this)
        this.handleEmployeesChange = this.handleEmployeesChange.bind(this)
	}

    handleMonthlyChange(e) {
		let val = e.target.value
		this.setState((prevState) => {
			return {
						monthly: val,
					}
		});
		this.setState((prevState) => {
			return {
						estimated: .3*prevState.monthly,
					}
		});
		this.setState((prevState) => {
			return {
						savings: prevState.employees*1337+prevState.estimated
					}
		});

    }
	handleEmployeesChange(e) {
		let val = e.target.value
		this.setState({employees: val});
		this.setState( prevState => {
			return {savings: prevState.employees*1337+prevState.estimated}
		});
	}
    render() {
        return <section className="Savings" >
                <div className="container">
                    <div className="columns is-tablet is-centered">
                        <div className="column has-text-left">
                            <div className="content">
                                <b className="is-size-4" > See how much you can save with Bellotero.io </b>
                                <p className="is-size-6" >
                                    With Bellotero.io you save time and money make real - time decisions that boost your business and your bottom line.Get less wrongfully blocked payments, save time on bookkeeping and no need to worry about safety.
                                </p>
                            </div>
							<b className="is-size-6">Monthly ingredient spending</b>
                            <div id="sliderWithValue" className="content">
                                <span className="is-size-4">${this.state.monthly}</span>
                                <input  className="slider has-output is-primary" onChange={this.handleMonthlyChange} min="10" max="100" value={this.state.monthly} step="1" type="range"/>
							</div>
							<b className="is-size-6">Full-time employees that process invoices</b>
                            <div id="slideremployees" className="content">
                                <span className="is-size-4">{this.state.employees}</span>
                                <input className="slider has-output is-primary" onChange={this.handleEmployeesChange} min="1" max="10" value={this.state.employees} step="1" type="range"/>
                            </div>
                        </div>
                        <div id="results" className="column">
							<div className="">
								<div id="estimated" className="">
									<span className="is-size-4">Your estimated annual savings</span>
									<p className="has-text-primary" onChange={this.handleEstimatedChange}>
										<small className="has-text-weight-light">$</small>
										{this.state.estimated.toFixed(2)}
									</p>
								</div>
								<div id="savings" className="">
									<span className="is-size-4">Estimated food <br/> cost savings</span>
									<p className="has-text-primary" >
										<small className="has-text-weight-light">$</small>
										{this.state.savings.toFixed(2)}
									</p>
								</div>
							</div>
						</div>
					</div>
                </div>
            </section>
    }
}

export default Saving;
