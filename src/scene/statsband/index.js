import React, {
    Component
} from 'react';
import { Parallax } from 'react-scroll-parallax';
import './statsband.scss';
import band1 from 'assets/band_1.jpg'
import band2 from 'assets/band_2.jpg'
import band3 from 'assets/band_3.jpg'

class Statsband extends Component {
    render() {
        return <section id = "Statsband" >
					<div className="columns">
						<div className="column is-paddingless">
							<Parallax
								className="bandColumn"
								offsetYMax={10}
								offsetYMin={-10}
								slowerScrollRate
								tag="figure">
								<div id="band1">
									<div style={{backgroundImage:`url(${band1})`}}>
									</div>
								</div>
							</Parallax>
							<p className="is-size-1">
								50<small>%</small><br/>
								<p>SAVED IN <br/> BOOKKEEPING COSTS</p>
							</p>
						</div>
						<div className="column is-paddingless">
							<Parallax
								className="bandColumn"
								offsetYMax={20}
								offsetYMin={-20}
								slowerScrollRate
								tag="figure">
								<div id="band2">
									<div style={{backgroundImage:`url(${band2})`}}>
									</div>
								</div>
							</Parallax>
							<p className="is-size-1">
								100<small>h</small><br/>
								<p>AND MORE SAVED IN <br/>BOOKKEEPING TIME</p>
							</p>
						</div>
						<div className="column is-paddingless">
							<Parallax
								className="bandColumn"
								offsetYMax={30}
								offsetYMin={-30}
								slowerScrollRate
								tag="figure">
								<div id="band3">
									<div style={{backgroundImage:`url(${band3})`}}></div>
								</div>
							</Parallax>
							<p className="is-size-1">
								50<small>%</small><br/>
								<p>DECREASE IN <br/>FOOD COSTS</p>
							</p>
						</div>
					</div>
            </section>
    }
}

export default Statsband;
