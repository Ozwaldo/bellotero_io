import React, {
    Component
} from 'react';
import './description.scss';
import right from 'assets/arrow-right.svg'
class Description extends Component {
    render() {
        return <section className="Description" >
                <div className="container">
					<div className="columns is-tablet is-centered">
						<div className="column">
							<div className="content has-text-left"> 
								<b className="is-size-4" > 
									Bellotero.io is the digital solution that <br className="is-hidden-mobile"/>
									gives you fast, accurate, automated accounts payable and smart, <br className="is-hidden-mobile"/>
									business-transforming insights.  
								</b> 
								<p className="is-size-4" >
									Get the full picture. <br className="is-hidden-mobile"/>
									In half the time.
								</p> 
								<p className="is-size-6" >
									Threads keep all your conversations clearly separated by topic so replies won’t get buried in an endless stream of group chat.
								</p> 
								<a className="is-size-6 has-text-primary arrow-right">Learn more<div style={{background:`url(${right})`}} className=""></div></a>
							</div>
						</div>
						<div className="column">
							<img className="" src={require('assets/intro-img-1.png')} alt="Description One" />
						</div>
					</div>
					<div className="columns is-tablet is-centered">
						<div className="column">
							<img className="" src={require('assets/intro-img-2.png')} alt="Description two" />
						</div>
						<div className="column">
							<div className="content has-text-left"> 
								<p className="is-size-4" >
									Timesaving, <br/> moneymaking.
								</p> 
								<p className="is-size-5" >
									Bellotero.io automatically turns your threaded conversations into a searchable catalog of topics.
								</p> 
								<a className="is-size-5 has-text-primary arrow-right">Learn more <div style={{background:`url(${right})`}} className=""></div></a>
							</div>
						</div>
					</div>
                </div>
            </section>
    }
}

export default Description;