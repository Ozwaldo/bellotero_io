import React, {
    Component
} from 'react';
import './navbar.scss';

class Home extends Component {
    render() {
        return <section id="navbar" className="">
                <nav className="navbar">
                    <div className="navbar-brand">
                        <a className="navbar-item" href="/">
                            <img id="logo" src={require('assets/bellotero-logo.svg')} alt="Bellotero.io" width="112" height="28"/>
                        </a>
                    <div className="navbar-burger burger" data-target="navbarMenu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div id="navbarMenu" className="navbar-menu">
                    <div className="navbar-start">
                        <a className="navbar-item is-hidden-desktop" href="#">Features</a>
                        <a className="navbar-item is-hidden-desktop" href="#">Solutions</a>
                        <a className="navbar-item is-hidden-desktop" href="#">Stories</a>
                        <a className="navbar-item is-hidden-desktop" href="#">Partner</a>
                        <a className="navbar-item is-hidden-desktop" href="#">About</a>
                        <a className="navbar-item is-hidden-desktop" href="#">Blog</a>
                    </div>
                    <div className="navbar-end">
                        <a className="navbar-item" href="#">Features</a>
                        <a className="navbar-item" href="#">Solutions</a>
                        <a className="navbar-item" href="#">Stories</a>
                        <a className="navbar-item" href="#">Partner</a>
                        <a className="navbar-item" href="#">About</a>
                        <a className="navbar-item" href="#">Blog</a>
                        <div className="navbar-item">
                            <div className="field is-grouped">
                                <p className="control">
                                    <a id="btnRequest" className="button is-primary" target="_blank" href="#">
                                        <span>
                                            Request Demo
                                        </span>
                                    </a>
                                </p>
                                <p className="control">
                                    <a id="btnLogin" className="button " href="#">
                                        <span>Log In</span>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </section>
    }
}

export default Home;