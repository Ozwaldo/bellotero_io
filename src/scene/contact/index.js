import React, {
    Component
} from 'react';
import './contact.scss';

class Contact extends Component {
    render() {
        return <section id="Contact" >
                <div className="container">
					<div className="columns is-tablet has-text-centered-mobile">
                    <div className="column">
                        <p className="is-size-2">Ready to get started  <br className="is-hidden-mobile"/> with Bellotero<span id="ioInpt">.io</span>?</p>
                    </div>
                    <div className="column">
                        <p className="is-size-4" >
                            No more manual data entry. <br/>
                            Hands off.Thumbs up.
                        </p> 
                        <div className="field  has-addons">
                            <div className="control is-expanded">
                                <input className="input" type="text" placeholder="Your email address"/>
                            </div>
                            <div className="control is-expanded">
                                <a className="button is-primary">
                                Request a demo
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </section>
    }
}

export default Contact;
