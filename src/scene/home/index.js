import React, {
    Component
} from 'react';
import './home.scss';

class Home extends Component {
    render() {
        return <section id="Home" >
                <div className="container">
                <div className="columns">
                    <div className="column">
                        <p className="is-size-3 has-text-left" >
                            <b className="" > Digitize your invoices  </b> <br/>
                            and create your own shopping cart.
                        </p> 
                        <div className="field has-addons">
                            <div className="control is-expanded">
                                <input className="input" type="text" placeholder="Your email address"/>
                            </div>
                            <div className="control is-expanded">
                                <a className="button is-primary">
                                Request a demo
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="column">
                        <img id="imgHome" src={require('assets/hero-mockup.png')} alt="Different displays" />
                    </div>
                </div>
                </div>
            </section>
    }
}

export default Home;