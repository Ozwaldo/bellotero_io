import React, {
    Component
} from 'react';
import './information.scss';

class Information extends Component {
    render() {
        return <section className="Information" >
                <div className="container">
                    <div className="columns is-mobile is-centered">
                        <div className="column">
                            <div className="content"> 
                                <h1 className="is-size-3" > Digitalize your invoices </h1> 
                                <p className="AppIntro" >
                                    and creat your aun shoping cart.
                                </p> 
                            </div>
                            <div className="content"> 
                                <p className="AppIntro" >
                                    lorem ispsum
                                </p> 
                            </div>
                        </div>
                        <div className="column">
                            <img className="AppLogo" alt = "logo" />
                        </div>
                    </div>
                </div>
            </section>
    }
}

export default Information;