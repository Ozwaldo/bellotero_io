import React, {
    Component
} from 'react';
import './brandband.scss';

const brands = [
    {src:require('assets/logo-1.png'), class:'is-hidden-mobile', name:'logo-1'},
    {src:require('assets/logo-2.jpg'), class:'', name:'logo-2'},
    {src:require('assets/logo-3.jpg'), class:'', name:'logo-3'},
    {src:require('assets/logo-4.jpg'), class:'', name:'logo-4'},
    {src:require('assets/logo-5.jpg'), class:'', name:'logo-5'},
    {src:require('assets/logo-6.jpg'), class:'is-hidden-mobile', name:'logo-6'}
]
class Brandband extends Component {
    render() {
        return <section id="Brandband" >
                <div className="container">
                {
                    brands.map( val => <img className={val.class} src={val.src}/>)
                }
                </div>
            </section>
    }
}

export default Brandband;
