import React, {
    Component
} from 'react';
import Swiper from 'react-id-swiper/lib/custom';
import 'react-id-swiper/src/styles/scss/swiper.scss';
import './carousel.scss';


class Carousel extends Component {
    render() {
        const params = {
            pagination: {
                el: '.swiper-pagination.swiper-pagination-black',
                type: 'bullets',
                dynamicBullets: true,
                clickable: true
            },        
            // navigation: {
            //     nextEl: '.swiper-button-next',
            //     prevEl: '.swiper-button-prev'
            // },        
            spaceBetween: 30
        }      
        return <section id="Carousel" >
                <div className="container">
                    <span className="title is-size-4">Our customers love us</span>
                    <Swiper {...params}>
                        <div>
                            <div className="columns is-tablet">
                                <div className="column is-three-fifths-tablet">
                                    <p>
                                        <img className="quotation" src={require('assets/quotation-marks.svg')} />
                                        It's funny what memory does, isn't it? My favorite holiday tradition might not have happened more than once or twice. But because it is such a good memory, so encapsulating of everything I love about the holidays, in my mind it happened every year. Without fail. 
                                    </p>
                                </div>
                                <div className="column is-two-fifths-tablet has-text-centered-mobile">
                                    <span className=" is-size-4 auth">
                                        Molly O’ Keefe
                                    </span> <br/>
                                    <span className="charge">American Author</span>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className="columns is-tablet is-centered">
                                <div className="column is-one-third">
                                    <img className="imgSlide" src={require('assets/band_2.jpg')} />
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className="columns">
                                <div className="column is-three-fifths-tablet">
                                    <p>
                                        <img className="quotation" src={require('assets/quotation-marks.svg')} />
                                        It's funny what memory does, isn't it? My favorite holiday tradition might not have happened more than once or twice. But because it is such a good memory, so encapsulating of everything I love about the holidays, in my mind it happened every year. Without fail. 
                                    </p>
                                </div>
                                <div className="column is-two-fifths-tablet has-text-centered-mobile">
                                    <span className=" is-size-4 auth">
                                        Molly O’ Keefe
                                    </span> <br/>
                                    <span className="charge">American Author</span>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className="columns is-tablet is-centered">
                                <div className="column is-one-third">
                                    <img className="imgSlide" src={require('assets/band_3.jpg')} />
                                </div>
                                <div className="column is-one-third">
                                    <img className="imgSlide" src={require('assets/band_1.jpg')} />
                                </div>
                            </div>
                        </div>
                    </Swiper>
                
                </div>
            </section>
    }
}

export default Carousel;