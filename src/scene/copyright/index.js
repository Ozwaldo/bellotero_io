import React, {
    Component
} from 'react';
import './copyright.scss';
import logoAppStore from 'assets/google-play.png'
import logoPlayStore from 'assets/app-store.svg'
class Copyright extends Component {
    render() {
        return <section id="Copyright" >
                    <div className="container">
                        <span>© 1909 Bellotero.io</span>
                        <div>
                            <a href="#">Privacy Policy</a>
                            <a href="#">Terms of Service</a>
                        </div>
                    </div>
            </section>
    }
}

export default Copyright;
