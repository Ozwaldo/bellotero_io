const HtmlWebPackPlugin = require("html-webpack-plugin");

const htmlWebpackPlugin = new HtmlWebPackPlugin({
    template: "./src/index.html",
    filename: "./index.html"
});
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require('path');

module.exports = {
    module: {
        rules: [{
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.css$/,
                use: [
                    // fallback to style-loader in development
                    process.env.NODE_ENV !== 'production' ? 'style-loader' : MiniCssExtractPlugin.loader,
                    "css-loader",
                    "postcss-loader"
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    // fallback to style-loader in development
                    process.env.NODE_ENV !== 'production' ? 'style-loader' : MiniCssExtractPlugin.loader,
                    "css-loader",
                    "postcss-loader",
                    "sass-loader"
                ]
			},
			{ test: /\.(png|jpg|svg)$/, loader: 'file-loader?name=images/[name].[ext]' },
            // {
            //     test: /\.sass$/,
            //     loader: "style-loader!css-loader!sass-loader!postcss-loader"
            // }
        ]
	},
	resolve: {
		alias: {
			assets: path.resolve(__dirname, 'src/assets/'),
			// Templates: path.resolve(__dirname, 'src/templates/')
		}
	},
    plugins: [htmlWebpackPlugin, new MiniCssExtractPlugin({
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        filename: "index.css",
        chunkFilename: "id.css"
    })
]
};
